public class Student{
	
	private int grade;
	private String name;
	private String course;
	private int amountLearnt;
	private int amountStudied;
	
	public Student(String name, int grade){
		this.grade = grade;
		this.name = name;
		this.course = "Math";
		this.amountLearnt = 0;
		this.amountStudied = 100;
	}
	
	public int getGrade(){
		return this.grade;
	}
	
	public void setGrade(int grade) {
		this.grade = grade;
	}
	
	public String getName(){
		return this.name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getCourse(){
		return this.course;
	}
	public void setCourse(String course) {
		this.course = course;
	}
	public int getAmountLearnt(){
		return this.amountLearnt;
	}
	//No set yet
	public int getAmountStudied(){
		return this.amountStudied;
	}
	public void setAmountStudied(int amountStudied){
		this.amountStudied = amountStudied;
	}
	
	
	public void study( int amountStudied){
	for(int i =0; i < amountStudied; i++){
		this.amountLearnt += 1;
	}
	}
	public void dropOut(){
		
		this.course = course + " dropped!";
		this.grade = -1;
	}
	
	
	
	public String toString(){
		
		return name + " " + grade + " " + course;
		
	} 

}
